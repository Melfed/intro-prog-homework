# -*- coding: utf-8 -*-
"""
Created on Tue Feb 27 19:23:40 2018

@author: 02012018
"""

from canine import dog
from feline import cat
from primate import human

dog.speak()
cat.speak()
human.speak()

from canine.dog import speak
speak()

#Exercise 1
from feline.cat import speak
speak()

#Exercise 2
from canine import dog