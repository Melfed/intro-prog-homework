# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 19:31:39 2018

@author: 02012018
"""


stock_prices = {
        'a': 1,
        'b': 2,
        'c': 3}


print(type(stock_prices.keys()), type(stock_prices), stock_prices.keys())

#Exercise 0
my_tuple = (1, 2, 3)
print(my_tuple)
print(my_tuple[2])
#The index of 1 is 0, the index of 2 is 1, the index of 3 is 2
#The second print gets the value in the tuple with the index 2

#Exercise 1
#It won't work because a tuple is not supposed to be changed

#Exercise 2
my_apple = (100, 101, 102, 103, 104)

#Exercise 3
'''
my_tuple.sort()
print(my_tuple)

Gives an error as tuple has no attribute 'sort', a tuple is designed to be as it is so even sorting is not possible
'''

#Exercise 4
apple_list = [100, 101, 102, 103, 104]
apple_list.append(99)
print(apple_list)
apple_list.sort()
print(apple_list)

#Exercise 5
#Tuple can't be changed, it is as it is forever. A List can be changed, sorted, added values, change values...

#Exercise 6
#If they are exactly equal in order, they will be equal. If not, either tuple or list, they will not be equal.
#Dictionaries will be equal even if order is different.

#Exercise 7
print(my_tuple)
my_list=list(my_tuple)
print(my_list)

#Exercise 8
my_dictionary = {'a':1, 'b':2, 'c':3, 'd':4}
print(my_dictionary.keys())
print(my_dictionary.values())
print(my_dictionary.items())

#Exercise 9
dictionary = {}
dictionary['hello'] = 'world'
print(dictionary)
    
#Exercise 10
stock_prices = {'AAPL': 100, 'GOOG': 50}
stock_interest = 'AAPL'
print(stock_prices[stock_interest])
print(stock_interest)
#stock_interest gets the value 'AAPL' so if we print the function of that will be the same as printing the value
#attributed to AAPL which is 100

#Exercise 11
a = tuple([1, 2, 3])
print(a)

#Exercise #12
my_dict = {'a': 1, 'b': 2}
# One way
my_dict.pop('a')
print(my_dict)

my_dict = {'a': 1, 'b': 2}
# Another way
del my_dict['b']
print(my_dict)

# In the first way the value of the 'a' key can be stored for later use
# In the second way is not possible to store the result


#Exercise 13
def  h(list):
    list.append('homie')
    return print(list)

h([1,2,3])

#Exercise #14
GOOG = 788
APPL = 3783
KPMG = 31
BCG = 1232
MSFT = 34
OSYS = 23
PHZR = 3

def sum_stocks(*stock):
    # This saves every argument in a tuple. In this case:
    # stock = (788, 3783, ..., 3)
    #See: http://introtopython.org/more_functions.html#Accepting-an-arbitrary-number-of-arguments
    return sum(stock)/len(stock)

print(sum_stocks(GOOG, APPL, KPMG, BCG, MSFT, OSYS, PHZR))

# With only one argument
def sum_stocks2(stock):
    return sum(stock)/len(stock)

print(sum_stocks2([GOOG, APPL, KPMG, BCG, MSFT, OSYS, PHZR]))