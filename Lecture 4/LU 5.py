a_list = [1,2,'asdf']

# Exercise #1

"""
● The number “1” True
● The float “1.1” True
● The number “0” False
● The float “0.0001” True
● The empty string “” False
● The null value “None” False
"""

# Exercise #2

def get_bool_ticker(ticker_symbol):
    if ticker_symbol == 'APPL':
        return True
    else:
        return False

print(get_bool_ticker('TSLA'))
print(get_bool_ticker('APPL'))

# Exercise #3

def gender_responde(gender):
    if gender == 'male':
        return 'You like pink'
    elif gender == 'female':
        return 'You hate pink'
    
print(gender_responde('male'))
print(gender_responde('female'))
print(gender_responde('other'))

# Exercise #4
"""
“This line shouldn’t be printed” is being printed because is outside the if
"Correct" function:
def gender_responde(gender):
if gender == 'male':
    return 'You like pink'
elif gender == 'female':
    return 'You hate pink'
else:
    return "This line shouldn’t be printed"
"""

# Exercise #5

def age_operations(age_mother, age_father):
    if age_mother == age_father:
        return age_mother + age_father
    else:
        return age_mother - age_father

print(age_operations(40,40))
print(age_operations(40,50))

# Exercise #6
def items_number_comparation(my_dict, my_list):
    if len(my_dict) == len(my_list):
        return 'awesome'
    else:
        return 'that is sad'

my_dict = {'a': 1, 'b': 2, 'c': 3}
my_list1 = [1, 2, 3]
print(items_number_comparation(my_dict, my_list1))

my_list2 = [1, 2, 3, 4]
print(items_number_comparation(my_dict, my_list2))

# Exercise #7
dict_tickers = { 'APPL': 100, 'GOOG': 200, 'FB': 300}

for key in dict_tickers.keys(): #or for key in dict_tickers:
    print(key)
    
for value in dict_tickers.values():
    print(value)
    
#or
for key in dict_tickers:
    print(dict_tickers[key])

for key in dict_tickers:
    print('the stock price of', key, 'is', dict_tickers[key])

#or
    
for key, value in dict_tickers.items():
    print('the stock price of', key, 'is', value)

# Exercise #8

my_tuple = (10, 20, 30)
my_list = [40, 50, 60]
my_dict = { 'APPL': 100, 'GOOG': 90, 'FB': 80}

for num in my_tuple:
    print(num)

for value in my_list:
    print(value)

for key in my_dict:
    print(my_dict[key])
    
#can actually put whatever I want in the key or value or num part, even an a will work

# Exercise #9
def mult_list_by_2(my_list):
    for index, value in enumerate(my_list):
        my_list[index] = value * 2
    return my_list

my_list = [1, 2, 3]
print(mult_list_by_2(my_list))

# Exercise #10

def dict_mult_by_two(my_dict):
    for key in my_dict:
        my_dict[key] = my_dict[key] * 2
    return my_dict

my_dict = { 'APPL': 100, 'GOOG': 90, 'FB': 80}
  
print(dict_mult_by_two(my_dict))

# Exercise #11

def list_number_comparison(my_list, my_number):
    if len(my_list) > my_number:
        return True
    else:
        return False

my_list = [1, 2, 3, 4]
my_number = 3
print(list_number_comparison(my_list, my_number))

my_list = [1, 2, 3]
print(list_number_comparison(my_list, my_number))
