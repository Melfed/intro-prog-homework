﻿# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 18:03:31 2018

@author: 02012018
"""

def print_trade(ticker_symbol='AAPL', num_stocks=5):
    print('ricado wants to trade',
          num_stocks, 'of', ticker_symbol)
    
print_trade()
print_trade(num_stocks=10)
print_trade(ticker_symbol='GOOG')
print_trade(num_stocks=10, ticker_symbol='SBE')
print_trade(ticker_symbol='SBE', num_stocks=10)

#Exercises

#Exercise 1
"""
Scope is where the variables and functions can be called, they were initialized inside the scope
Local scope is the scope within a function, variable initialized inside a function will only exist in the
scope of that function and not for the whole file
Global scope is the scope of the whole file, variables can be in the global and local scope if initialized in
the global scope and then called inside a function, but can't be initialized in the local scope and then belong
as well to the global scope
"""

#Exercise 2
"""
first_var = global
who = local inside function hello_world
second_var = global
who_again = local inside function who_again
"""

#Exercise 3
#It will give a NameError output as the variable used in the function is initialized after the function

#Exercise 4
variable_1 = 2
def function():
    double = 2 * variable_1
    return double

variable_2 = function()

print(variable_1)
print(variable_2)

#Exercise 5
"""
The error is the print(_power_) as we are calling a funtion defined in the local scope of the function and not
in the global scope so this will give a NameError
With taking that print out, the code won't give an error anymore and print(a) will print 2^10=1024
"""

#Exercise 6
"""
This will print 10, 10, 1024 as the first print brings the power first defined in the global scope, just
like the second print as it is a print in the global scope so the only power defined in the global scope equals
10. Then, the third print will print the function and this function has power defined in her local scope which
will print 1024
"""

#Exercise 7
_what = 'coins'
_for = 'chocolates'

def transaction(_what, _for):
    _what = 'oranges'
    _for = 'bananas'
    print(_what, _for)
    
transaction(_what, _for)
print(_what, _for)

#The function transaction will go with _what and _for being the first defined globally coins and chocolates.
#But the function will then change the _what and the _for to oranges and bananas and print that
#The second print just searches for the globally initialized and defined as coins and chocolates

#Exercise 8
#Same as exercise 1

#Exercise 9
def name(first_name = 'Miguel', last_name = 'Ferrão'):
    print(first_name, last_name)
    
name()
name(last_name = 'Ferrolas', first_name = 'Mike')
name('Miky', 'Ferras')

#Exercise 10
first_name = 'Miguel'
last_name = 'Ferrão'

def name(first_name = 'john', last_name = 'hehe'):
    print(first_name, last_name)
    
name()
name(first_name = first_name, last_name = last_name)
name(first_name = 'hihi', last_name = 'lool')

#As expected the first name prints the default values, the second prints the values assigned in the global 
#scope and the last name prints the values it is defining for the keyword arguments

#Exercise 11

def greetings(message = 'Good morning', name = ''):
    print(message, name)
    
greetings()
greetings('You know nothing', 'John Snow')
