# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 18:43:39 2018

@author: 02012018
"""

def divide_by_two(n):
    ret_val = n / 2
    return ret_val

a = 2
b = divide_by_two(a)
print('The value of a/2 is', b)


#CLASS 2 LU 2

#Exercise 1

power = a ** b

#Exercise 2

def are_equal(a, b):
    are_they = a == b
    return are_they

print(are_equal(5, 5))
print(are_equal(5, 4))

#Exercise 3

def hypotenuse(a, b):
    triangle = a ** 2 + b ** 2
    return triangle ** -2

print(hypotenuse(1, 1))
print(hypotenuse(2, 2))

#Exercise 4

bitcoin = 8095

def ethereum_bitcoin(a):
    ethereum = a * bitcoin / 10.217524
    return ethereum

print(ethereum_bitcoin(1))

def litecoin_bitcoin(a):
    litecoin = a * bitcoin / 54.496143
    return litecoin

print(litecoin_bitcoin(1))

def euros_bitcoin(a):
    euros = a * bitcoin / 1.23637
    return euros

print(euros_bitcoin(1))

#Exercise 5

def divide_by_two(n):
    ret_val = n / 2
    return ret_val

q = divide_by_two(4)

def divide_by_two2(n):
    ret_val = n / 2

r = divide_by_two2(4)
print(divide_by_two2(4))

#If the function doesn’t have return, it won’t return a value thus not having a value and having no type

def simple_func():
    ma = 1
    ba = 2
    return 100

print(simple_func())

#The function may have other things in the machinery, but its return is 100 so if we print it is gives its value
#Print alone returns null

#Exercise 6

def foo(y):
    print ('hello')
    return y + 1
    print ('this place in code will never get reached :(')
print (foo(5))
# hello
# 6

def bar():
    return # implicit return None
print (bar()) is None
# True
"""
def baz(y):
    x = y * 2
    # implicit return None
z = baz()
print (z) is None
# True

#return won't print anything, but gives a value to the function you are defining
#print only prints the thing, does nothing more than that

#Exercise 7

a = 5

print(a)
5

a
Out[60]: 5

#REPL will print a red [out] when we are calling just a variable and not a function
#if a function is called, such as print, the red [out] won't appear
"""
