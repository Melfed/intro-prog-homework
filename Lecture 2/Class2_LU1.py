# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 18:38:06 2018

@author: 02012018
"""

print('olá mundo')

#CLASS 2 LU 1

#Exercise 1

stock_name = 'AMZN'
stock_value = 1405.54
stock_number = 10
stock_action = 'buy'

print('I would like to', stock_action, stock_number, 'shares in', stock_name)

#Exercise 2

person1 = 'Miguel'
person2 = 'João'
borrow_value = stock_value * stock_number

print(person1, 'needs to borrow', borrow_value, 'euros from',
      person2, 'in order to trade', stock_number, 'stocks in', stock_name)

#Exercise 3

number1 = 5
number2 = 10
number3 = (number1 * number2)

#Exercise 4

celsius = 30
fahrenheit = celsius * 33.8
kelvin = celsius * 274.15

print(celsius)
print(fahrenheit)
print(kelvin)