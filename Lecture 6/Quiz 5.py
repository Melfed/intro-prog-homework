# -*- coding: utf-8 -*-
"""
Created on Tue Mar 13 17:56:59 2018

@author: 02012018
"""

# Quiz 5

# PLEASE READ THE INSTRUCTIONS:

# BE EXTREMELY CAREFUL WITH THE NAMES OF THE FUNCTIONS!!!!
# Your functions should have exactly the same names as the ones 
# on the description!

# If you don't name the functions well, your code will not pass the tests! 

# Check the first example to understand exactly how to do it.

# EXERCISE ONE
# Write a function called stock_fun that takes a single argument. This function should
# return True if the value of the argument is 'GOOGLE' or 'TSLA'.
# Return False otherwise

def stock_fun(stock_name):
    if stock_name == 'GOOGLE' or stock_name == 'TSLA':
        return True
    else:
        return False
    

# EXERCISE TWO
# Write a function called comparing_ages that takes two arguments: 
# The first argument should be your age 
# The second should be the age of your mother. 

# The function should Return True if the difference between your age and your 
# mother's is bigger than 20. Return False otherwise.

def comparing_ages(my_age, age_mother):
    if my_age - age_mother > 20:
        return True
    else:
        return False
    

# EXERCISE THREE
# Write a function called list_slice that takes a list as an argument and returns the second 
# element of that list. Define a list [1,2,3] as the default value for your argument 
        
def list_slice(my_list=[1,2,3]):
    return my_list[1]


# EXERCISE FOUR
# Write a function called double_dict that takes a single dictionary as an argument that maps a 
# string to a number. The function should return a new dictionary that multiplies
# each value of the input dictionary by 2

def double_dict(my_dict):
    for key in my_dict:
        my_dict[key] = my_dict[key] * 2
    return my_dict
